<!-- Ce fichier permet de charger le graphique "Accidents par mois" une fois inclus au bon endroit  -->
<div class="col col-md-12 col-lg-6" style="background-color : white; border-style: inset;">
<br/><p> Sexe des conducteurs </p>
    <div><canvas id="myChart"></canvas></div> <!-- ce canvas permet l'affichage du graphique dans la page -->
    <?php
    $H = $DA->get_number_sexe($aff_name_department, "H");
    $F = $DA->get_number_sexe($aff_name_department, "F");
    $array_with_data = array(100 * $H / ($H + $F), 100 * $F / ($H + $F));
    ?>
    <script> /**
     * @brief crée le graphique avec chart JS pour l'afficher ensuite dans le canvas
     */
        var ctx = document.getElementById("myChart");

        var myChart = new Chart(ctx, {
            type: "doughnut",
            data: {
                labels: ["Hommes", "Femmes"],
                datasets: [{
                    data: <?php echo $data = json_encode($array_with_data); ?>,
                    backgroundColor: [
                        "#00e676",
                        "#e91e63",

                    ],
                    borderWidth: 0.5,
                    borderColor: "#ddd",
                }, ],
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                title: {
                    display: true,
                    text: "Sexe des conducteurs accidentés",
                    position: "top",
                    fontSize: 16,
                    fontColor: "#111",
                },
                legend: {
                    display: true,
                    position: "bottom",
                    labels: {
                        boxWidth: 20,
                        fontColor: "#111",
                        padding: 15,
                    },
                },
                tooltips: {
                    enabled: false,
                },
                plugins: {
                    datalabels: {
                        color: "#111",
                        textAlign: "center",
                        font: {
                            lineHeight: 1.6,
                        },
                        formatter: function(value, ctx) {
                            return (
                                ctx.chart.data.labels[ctx.dataIndex] +
                                "\n" +
                                value +
                                "%"
                            );
                        },
                    },
                },
            },
        });
    </script>
</div>