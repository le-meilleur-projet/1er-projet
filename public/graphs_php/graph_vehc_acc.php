<!-- Ce fichier permet de charger le graphique "Accidents par type de véhicules" une fois inclus au bon endroit  -->
<div class="col col-md-12 col-lg-6" style="background-color : white; border-style: inset;">
    <p> Accidents par type de véhicules </p>
    <div  style = "height : 75%"><canvas id="Chart_bar" ></canvas></div> <!-- ce canva permet l'affichage du graphique dans la page -->
    <?php
    $array_with_data = $DA->get_number_vehicleaccident($aff_name_department);
   
    ?>
   <script>/**
     * @brief crée le graphique avec chart JS pour l'afficher ensuite dans le canvas
     */
        var ctx = document.getElementById("Chart_bar");

        var Chart_bar = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['2 roues petite puissance', 'Voitures et utilitaires', '2 roues grosse puissance', 'Transports en commun', 'Poids Lourds',  'Autre'],
        datasets: [{
            label: 'véhicules accidentés',
            data: <?php echo $data = json_encode($array_with_data); ?>,
            backgroundColor: [
                'rgba(216, 27, 96, 1)',
                'rgba(3, 169, 244, 1)',
                'rgba(255, 152, 0, 1)',
                'rgba(29, 233, 182, 1)',
                'rgba(156, 39, 176, 1)',
                'rgba(84, 110, 122, 1)'
            ],
            borderColor: [
                'rgba(216, 27, 96, 1)',
                'rgba(3, 169, 244, 1)',
                'rgba(255, 152, 0, 1)',
                'rgba(29, 233, 182, 1)',
                'rgba(156, 39, 176, 1)',
                'rgba(84, 110, 122, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        responsive: true,
        maintainAspectRatio: true,
        legend: {
            display: false
        },
        title: {
            display: true,
            text: 'Types de véhicules accidentés',
            position: 'top',
            fontSize: 16,
            padding: 20
        },
        scales: {
            yAxes: [{
                ticks: {
                    min: 100
                }
            }]
        }
    }
});
    </script>
</div>