<!-- Ce fichier permet de charger le graphique "Accidents par mois" une fois inclus au bon endroit  -->
<div class="col col-md-12 col-lg-6" style="background-color : white; border-style: inset;">
<br/> <p> Accidents par mois </p>
   <div  style = "height : 75%"> <canvas id="Chart_line" ></canvas> </div> <!-- ce canvas permet l'affichage du graphique dans la page -->
    <?php
    $array_with_data = $DA->get_nbacc_mois($aff_name_department);
    ?>
   <script>/**
     * @brief crée le graphique avec chart JS pour l'afficher ensuite dans le canvas
     */
        var ctx = document.getElementById("Chart_line");

        var Chart_bar = new Chart(ctx, {
            type: 'line',
            data: {
        labels: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai',  'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        datasets: [{
            label: 'Nombre d\'accidents',
            data: <?php echo $data = json_encode($array_with_data); ?>,
            backgroundColor: 'rgba(255, 152, 0, 1)',
            borderColor:  'rgba(255, 152, 0, 1)',
            borderWidth: 1
        }]
    },
    options: {
        responsive: true,
        maintainAspectRatio: true,
        legend: {
            display: false
        },
        title: {
            display: true,
            text: 'Types de véhicules accidentés',
            position: 'top',
            fontSize: 16,
            padding: 20
        },
    }
});
    </script>
</div>