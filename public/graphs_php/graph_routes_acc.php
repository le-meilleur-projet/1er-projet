<!-- Ce fichier permet de charger le graphique "Accidents par type de route" une fois inclus au bon endroit  -->
<div class="col col-md-12 col-lg-6" style="background-color : white; border-style: inset;">
<br/><p> Accidents par type de route </p>
    <div><canvas id="Chart_Dough"></canvas></div> <!-- ce canvas permet l'affichage du graphique dans la page -->
    <?php
    $array_with_data = $DA->get_Routes($aff_name_department);
    ?>
    <script>/**
     * @brief crée le graphique avec chart JS pour l'afficher ensuite dans le canvas
     */
        var ctx = document.getElementById("Chart_Dough");

        var myChart = new Chart(ctx, {
            type: "doughnut",
            data: {
                labels: ["Autoroute", "Autre", "Departementale et nationale", "Parking", "Zone Urbaine"],
                datasets: [{
                    data: <?php echo $data = json_encode($array_with_data); ?>,
                    backgroundColor: [
                        'rgba(216, 27, 96, 1)',
                        'rgba(3, 169, 244, 1)',
                        'rgba(255, 152, 0, 1)',
                        'rgba(29, 233, 182, 1)',
                        'rgba(156, 39, 176, 1)',
                        'rgba(84, 110, 122, 1)'

                    ],
                    borderWidth: 0.5,
                    borderColor: "#ddd",
                }, ],
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                title: {
                    display: true,
                    text: "Sexe des conducteurs accidentés",
                    position: "top",
                    fontSize: 16,
                    fontColor: "#111",
                },
                legend: {
                    display: true,
                    position: "bottom",
                    labels: {
                        boxWidth: 20,
                        fontColor: "#111",
                        padding: 15,
                    },
                },
                tooltips: {
                    enabled: false,
                },
                plugins: {
                    datalabels: {
                        color: "#111",
                        textAlign: "center",
                        font: {
                            lineHeight: 1.6,
                        },
                        formatter: function(value, ctx) {
                            return (
                                ctx.chart.data.labels[ctx.dataIndex] +
                                "\n" +
                                value +
                                "%"
                            );
                        },
                    },
                },
            },
        });
    </script>
</div>