<?php

namespace App\Http\Controllers;

$path = $_SERVER['DOCUMENT_ROOT'];
include "config/config_site.php"; ///appelle la configuration du titre de la page web


$page_nom = "Accidents par départements";

/// cette liste permet de vérifier que le département demandé par la page précédente est au bon format et fait bien parti des départements de la région
/// le if suivant redirige l'utilisateur si le département n'est pas renseigné comme il se doit
$liste_dep = array(1 => "Isère", "Savoie", "Haute-Savoie", "Ain", "Rhône", "Loire", "Haute-Loire", "Puy-de-Dôme", "Cantal", "Ardèche", "Allier", "Drôme");
if (!(in_array($_GET['department'], $liste_dep))) {
	header('Location: /');
	exit();
}

use App\Http\Controllers\dataAccidents;

$DA = new DataAccidents(); /// instanciation d'un objet de la classe DataAccidents

$page_nom = "Accidents par départements";
$aff_name_department = $_GET["department"];

?>
<!Doctype HTML>
<html>

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1" />
	<title><?php echo $site_nom; ?> - <?php echo $page_nom; ?></title>
	<link rel="shortcut icon" href="<?php echo asset('img/favicon.ico') ?>">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="<?php echo asset('css/style.css') ?>" type="text/css">
	<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
</head>

<body>
	<!-- Haut de page -->
	<?php include "templates/header.php"; /// inclusion du Header?>
	<div class="contenu_page"> <!-- nouveau conteneur pour afficher les données écrites et la carte du département -->
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-12">
					<h1 class="data_h1"><?php echo $aff_name_department;	?></h1>
					<br /><br />
				</div>
				<?php $SL->loadDep($aff_name_department); ?>
				<div class="col-md-12 col-lg-6 ">
					<p class="data_answer">Nombres d'accidents : <?php echo $DA->get_number_accident($aff_name_department); ?></p>
					<p class="data_answer">Pourcentage d'accidents de la région : <?php echo (int) $DA->get_quantity_region($aff_name_department); ?> %</p>
					<p class="data_answer">Âge moyen des conducteurs impliqués : <?php echo $DA->get_average_age($aff_name_department); ?> ans</p>
					<p class="data_answer">Nombre de morts : <?php echo $DA->get_number_human_damage($aff_name_department, "Tué"); ?></p>
					<p class="data_answer">Nombre de blessés : <?php echo $DA->get_number_human_damage($aff_name_department, "Blessé"); ?></p>
					<p class="data_answer">Nombre de personnes indemnes : <?php echo $DA->get_number_human_damage($aff_name_department, "Indemme"); ?> </p>

					</p>
				</div>
			</div>
		</div>
		<br />
		<br />
		<div class="container-fluid"> <!-- container pour affichage dynamique des graphs -->
			<div class="row justify-content-center"> 
				<?php include "graphs_php/graph_vehc_acc.php"; /// appel des graphs
				include "graphs_php/graph_H_F.php";
				include "graphs_php/graph_mois.php";
				include "graphs_php/graph_routes_acc.php";
				?>

			</div>
		</div><br />
	</div>

	<!-- Bas de page -->
	<?php include "templates/footer.php"; /// inclusion du footer?>
</body>

</html>
