<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include "config/config_site.php"; ///appelle la configuration du titre de la page web
$page_nom = "Accidents par régions";
?>
<!Doctype HTML>
<html>

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1" />
	<title><?php echo $site_nom; ?> - <?php echo $page_nom; ?></title>
	<link rel="shortcut icon" href="<?php echo asset('img/favicon.ico') ?>">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="<?php echo asset('css/style.css') ?>" type="text/css">
</head>

<body>
	<!-- Haut de page -->
	<?php include "templates/header.php"; ///chargement du header 
	?>
	<div class="container-fluid" style="height: 80%;">
		<div class="row justify-content-center" style="height: 100%;">
			
				<div class="col-md-12 col-lg-6" style="height : 100%">
					<?php $SL->loadRegion(); /// chargement de la carte des régions
					?>
				</div>
			
		</div>

	</div>
	
	<br />
	<!-- Bas de page -->
	<?php include "./templates/footer.php"; ///chargement du footer 
	?>
</body>

</html>