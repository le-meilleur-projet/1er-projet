<h1> Projet tutoré 2021 de Romain & Nabil & Sarah & Nicolas & Matthias​ </h1>

<h2>Modules :</h2>
<p> Ce projet est basé sur le <a href = "https://laravel.com/" > framework PHP Laravel 8 </a>, utilise <a href =  "https://getbootstrap.com/" > Bootstrap Twitter </a> pour le CSS et <a href = "https://www.chartjs.org/"> ChartJS </a> pour afficher les graphiques.</p> 
<p>Ce site WEB communique avec une Base de Données <a href = "https://www.postgresql.org/"> PostgreSQL </a> grâce aux classes de Laravel</p>
<p>
Vous pouvez accéder à ce site WEB en suivant cet URL <a href = "https://rg-nas.ddns.net:82"> https://rg-nas.ddns.net:82 </a> </p>


<h2> Contexte </h2>
<p>En 2019, plus de 56’000 accidents de la route ont été recensés, dont 5% avec des conséquences mortelles. La conduite est un moyen de transport extrêmement important au XXIe siècle - si ce n’est le plus important - et se doit ainsi d'être sécurisée. 
Il est donc intéressant de se pencher sur les causes de ces accidents, afin de comprendre comment et pourquoi ils ont eu lieu. Puis de mettre en avant les facteurs augmentant les chances d’accident : nos préjugés sont-ils vrais ? Ignorons-nous certains détails pourtant importants ?
</p>
<p>En nous intéressant aux accidents de la route ayant eu lieu dans la région Auvergne-Rhône-Alpes en 2019, nous nous demanderons : “Quels sont les facteurs naturels qui influencent les accidents routiers dans la région Auvergne Rhônes Alpes ?” </p>

<p>Le but de ce projet est de créer une page web qui affichera de manière simple les réponses à la problématique.
Pour ce faire, nous utiliserons <a href = "https://www.data.gouv.fr/fr/datasets/bases-de-donnees-annuelles-des-accidents-corporels-de-la-circulation-routiere-annees-de-2005-a-2019/" > un jeu de données du site data.gouv.fr et nous intéresserons plus précisément à l'année 2019 </a>.
Le site communiquera avec ladite base de données et fournira à l'utilisateur les statistiques des accidents et de leur gravité en fonction de certains facteurs tels que l'âge, le sexe ou les conditions atmosphériques. Ceci nous permettra d'étudier leur impact individuellement et d'éventuellement établir des corrélations entre ces facteurs et la gravité des accidents observés. 
Ce site est en 2 pages : une affichant la carte de la région et l’autre affichant les statistiques par départements. </p>

<h2> Arborescence du code : </h2>
<p> La plupart des fichiers de ce repo ont été créés automatiquement lors de la création du projet Laravel.<br/>
Le fichier .env à la racine du document contient les paramètres du projet. <br/>
Les ressources utilisés par le site WEB sont toutes localisées dans le dossier /publlic. <br/>
Le fichier contenant la classe SVGLoader est /app/classes/SVGLoader.php <br/>
Le fichier contenant la classe gérant les reuêtes avec la base de donnée est /app/Http/controllers/dataAccidents.phg </p>
<p> Les fichiers contenant les deux pages WEB sont region.blade.php et data.blade.php situés dans /resources/views/. Elles se situent à cet endroit car elles sont accessibles grâce à une vue qui redirige un URL simple vers ces deux pages.</p>
