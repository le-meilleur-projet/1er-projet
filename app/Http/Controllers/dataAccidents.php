<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @class Cette classe contient les fonctions permettant de récupérer les données de la base de données
 */
class dataAccidents extends Controller
{
    /**
     * @brief Permet de récupérer le nombre d'accidents d'un départements
     * @parameter prend le nom du département concerné
     * @return int nombre d'accidents
     */
    function get_number_accident($nom_department)
    {
        $requete = DB::select('SELECT COUNT(DISTINCT (Num_Acc)) as valeur FROM Accidents.dCarac WHERE dep = :id', ['id' => $nom_department]); //Requête
        foreach ($requete as $requete) {
            $result = $requete->valeur;
        }
        return $result;
    }

    /**
     * @brief Permet de récupérer le nombre d'hommes ou de femmes conducteur impliqués dans un accident
     * @parameter prend le nom du département concerné et le sexe sous la forme H ou F
     * @return int nombre d'hommes / de femmes
     */
    function get_number_sexe($nom_department, $sexe)
    {
        $requete = DB::select('SELECT nb FROM user_sexe WHERE dep = :dep AND sexe = :sexe', ['dep' => $nom_department, 'sexe' => $sexe]);
        foreach ($requete as $requete) {
            $result = $requete->nb;
        }
        return $result;
    }

    /**
     * @brief Permet de récupérer la moyenne d'âge des conducteurs impliqués dans un accident
     * @parameter prend le nom du département concerné
     * @return int moyenne d'age des conducteurs impliqués 
     */
    function get_average_age($num_department)
    {
        $requete = DB::select('SELECT AVG(U.an_nais) as valeur FROM Accidents.dUsers U JOIN (SELECT num_acc as na FROM Accidents.dCarac WHERE dep = :dep) C ON U.num_acc = C.na WHERE U.an_nais BETWEEN 1925 AND 2005 AND U.catu = \'Conducteur\'', ['dep' => $num_department]); //Requête
        foreach ($requete as $requete) {
            $result = $requete->valeur;
        }
        $result = 2019 - (int) $result;
        return $result;
    }

    /**
     * @brief Permet de récupérer le nombre d'accidents de chaque mois dans un départment
     * @parameter prend le nom du département concerné
     * @return int[] renvoie le nombre d'accidents par moi dans un tableau numéroté de 0 (janvier) à 11 (décembre)
     */
    function get_nbacc_mois($nom_department)
    {
        $requete = DB::Select('SELECT mois, COUNT(mois) as nb FROM Accidents.dCarac WHERE dep = :dep GROUP BY mois ORDER BY mois ASC', ['dep' => $nom_department]);
        foreach ($requete as $requete) {
            $result[] = $requete->nb;
        }
        return $result;
    }
    
    /**
     * @brief Permet de récupérer le nombre d'accidents par type de routes
     * @parameter prend le nom du département concerné
     * @return int[] renvoie le nombre d'accidents par type de routes dans un tableau (les résulats sont organisés par ordre alphabétique de la catégorie de route)
     */
    function get_Routes($nom_department)
    {
        $requete = DB::Select('SELECT catr, COUNT (catr) as nb FROM Accidents.dLieux WHERE num_acc IN (SELECT num_acc FROM Accidents.dCarac WHERE dep = :dep) GROUP BY catr ORDER BY catr ASC', ['dep' => $nom_department]);
        foreach ($requete as $requete) {
            $result[] = $requete->nb;
        }
        return $result;
    }

    /**
     * @brief Permet de récupérer la proportion d'accidents du département par rapport au nombre d'accidents de la région
     * @parameter prend le nom du département concerné
     * @return float renvoie le pourentage d'accidents du département par rapport au nombre d'accidents de la région
     */
    function get_quantity_region($nom_department){
        $rdep = DB::select('SELECT COUNT(num_acc) as nb FROM Accidents.dCarac WHERE dep = :dep', ['dep' => $nom_department]);
        $rall = DB::select('SELECT COUNT(num_acc) as nb FROM Accidents.dCarac');
        foreach ($rdep as $rdep) {
            $dep = $rdep->nb;
        }
        foreach ($rall as $rall) {
            $reg = $rall->nb;
        }
        return 100 * $dep / $reg;
    }


    /**
     * @brief Permet de récupérer le nombre de personne accidenté en fonction de la gravité de leur état clinique
     * @parameter prend le nom du département concerné et la gravité sous la forme "Tué" "Blessé" "Indemme"
     * @return int renvoie le nombre de personne
     */
    function get_number_human_damage($nom_department, $grav)
    {
        $requete = DB::select('SELECT valeur FROM human_damage WHERE dep = :dep AND grav = :gr', ['dep' => $nom_department, 'gr' => $grav]); //Requête

        foreach ($requete as $requete) {
            $result = $requete->valeur;
        }
        return $result;
    }



    
/**
     * @brief Permet de récupérer le nombre d'accidents par type de véhicules
     * @parameter prend le nom du département concerné
     * @return int[] renvoie le nombre d'accidents par type de véhicules dans un tableau (les résulats sont organisés par ordre alphabétique de la catégorie de véhicules)
     */
    function get_number_vehicleaccident($nom_department)
    {
        $requete = DB::select('SELECT nb FROM Accidents.vehc_impliques WHERE dep = :dep ORDER BY catv DESC', ['dep' => $nom_department]); //Requête


        foreach ($requete as $requete) {
            $result[] = $requete->nb;
        }
        return $result;
    }
}
